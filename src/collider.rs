/// List of possible collider shapes
pub enum Collider {
    Circular,
    Rectangle,
}
