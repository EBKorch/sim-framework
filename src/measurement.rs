/// Measurement class holds a list of time-value pairs
///
/// This is outh to represent a physical measurement result, where we take the 
/// current value of a specific signal with a predefined frequency.
///
/// The structure is quite lightweight, so the the measured value with the
/// timestamp must be supplied - then the measurement instance will decide
/// wheter or not it will measure anything.
pub struct Measurement<T: Clone, P> {
    frames: Vec<Frame<T>>,
    samplingtime: f64,
    last_time: f64,
    origin: P,
    active: bool,
}

impl<T: Clone, P> Measurement<T, P> {
    pub fn new(origin: P, samplingtime: f64) -> Self {
        Measurement {
            frames: vec![],
            samplingtime,
            origin,
            active: true,
            last_time: 0.0,
        }
    }

    pub fn measure(&mut self, value: T, time: f64) {
        if self.frames.len() < 1 || (time >= self.last_time + self.samplingtime) {
            if self.active {
                self.frames.push(Frame {
                    value,
                    time,
                });
                self.last_time = time;
            }
        }
    }

    pub fn suspend(&mut self, time: f64) -> bool {
        if self.active {
            self.last_time = time;
            self.active = false;
            true
        } else { false }
    }

    pub fn resume(&mut self, time: f64) -> bool {
        if !self.active {
            self.active = true;
            self.last_time = time;
            false
        } else { true }
    }

    pub fn origin(&self) -> &P {
        &self.origin
    }

    pub fn samplingtime(&self) -> &f64 {
        &self.samplingtime
    }

    pub fn values(&self) -> Vec<&T> {
        self.frames.iter().map(|frame| &frame.value).collect()
    }

    pub fn values_by_count(&self, count: usize) -> Vec<&T> {
        self.frames_by_count_iter(count).map(|frame| &frame.value).collect()
    }

    pub fn timestamps(&self) -> Vec<&f64> {
        self.frames.iter().map(|frame| &frame.time).collect()
    }

    pub fn timestamps_by_count(&self, count: usize) -> Vec<&f64> {
        self.frames_by_count_iter(count).map(|frame| &frame.time).collect()
    }

    pub fn frames(&self) -> &Vec<Frame<T>> {
        &self.frames
    }

    pub fn frames_by_count(&self, count: usize) -> Vec<&Frame<T>> {
        self.frames_by_count_iter(count).collect()
    }

    fn frames_by_count_iter(&self, count: usize) -> impl Iterator<Item=&Frame<T>> {
        if count >= self.frames.len() {
            self.frames.iter()
        } else {
            self.frames[self.frames.len()-count..].iter()
        }
    }

    /// Returns with frames which are not further from the most current frame
    /// than `elapsed_time`
    pub fn frames_by_elapsed_time(&self, elapse_time: f64) -> Vec<&Frame<T>> {
        // Initialize the list in which we will collect the frames
        let mut frames = vec![];
        // Get how many frames we have
        let length = self.frames.len();
        // We must check if we have any frame, as the logic depends on having at
        // least one frame
        if length > 0 {
            // Get the most current frame time - the distance of collected
            // frames from it should not be more than `elapsed_time`.
            let current_time = self.frames[length-1].time;
            // Iterate through every frame: however the loop will brake when we
            // encounter a frame which is outside the defined range
            for index in 1..length+1 {
                let frame = &self.frames[length-index];
                // Brake the loop if the time condition is met
                if current_time - frame.time > elapse_time {
                    break;
                } else {
                    // Otherwise append it to the collected frames list
                    frames.push(frame);
                }
            }
            frames.reverse();
        }
        // Return with the collected frames: this will be an empty array if we
        // had no frames in the measurement.
        frames
    }

    /// Removes frames from measurement, but keeps the most recent ones
    ///
    /// You can specify how many frames you want to keep. After running this
    /// method at most `keep_only` number of frames will be in the measurement.
    ///
    /// Returns with the removed frames.
    pub fn clean(&mut self, keep_only: usize) -> Vec<Frame<T>> {
        // If we intend to preserve more frames than available, we do nothing as
        // the `keep_only` count is not exceeded.
        if keep_only >= self.frames.len() {
            // We removed nothing, we return nothing
            vec![]
        } else {
            // Otherwise we remove any other frame over the `keep_only` limit. 
            self.frames.drain(0..self.frames.len()-keep_only).collect()
        }
    }

    /// Removes frames older than the specified time
    pub fn clean_by_time(&mut self, keep_only: f64) -> Vec<Frame<T>> {
        let length = self.frames.len();
        if length > 1 {
            let barrier = self.frames[length-1].time - keep_only;
            let mut index = 0;
            while self.frames[index].time < barrier { index += 1; }
            self.frames.drain(0..index).collect()
        } else {
            vec![]
        }
    }

    /// Removes every frame from the measurement
    ///
    /// Return value will contain every measured frame
    pub fn drain(&mut self) -> Vec<Frame<T>> {
        self.frames.drain(0..).collect()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Frame<T: Clone> {
    pub value: T,
    pub time: f64,
}

impl<T: Clone> Frame<T> {
    pub fn new(value: T, time: f64) -> Self {
        Frame { value, time }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Property {
    String(String),
    Float(f64),
    Integer(i32),
    Logical(bool),
}

#[cfg(test)]
mod tests {
    use super::*;

    fn apply_frames<T: Clone>(frames: &Vec<Frame<T>>, samplingtime: f64) -> Measurement<T, String> {
        let mut meas: Measurement<T, String> = Measurement::new("origin".to_string(), samplingtime);
        for frame in frames {
            meas.measure(frame.value.clone(), frame.time)
        }
        meas
    }

    fn pass_frames_through<T: Clone>(frames: &Vec<Frame<T>>, samplingtime: f64) -> Vec<Frame<T>> {
        apply_frames(frames, samplingtime).frames().iter().cloned().collect()
    }

    #[test]
    fn new_measurement() {
        let origin = "Origin".to_string();
        let meas: Measurement<f64, String> = Measurement::new(origin.clone(), 0.0);
        assert_eq!(meas.samplingtime(), &0.0);
        assert_eq!(meas.origin(), &origin);
    }

    #[test]
    fn values_f64() {
        let frames: Vec<Frame<f64>> = vec![
            Frame::new(0.0, 0.0), Frame::new(1.0, 0.1), Frame::new(2.0, 0.2), Frame::new(3.0, 0.3)
        ];
        assert_eq!(pass_frames_through(&frames, 0.0), frames);
    }

    #[test]
    fn values_i32() {
        let frames: Vec<Frame<i32>> = vec![
            Frame::new(0, 0.0), Frame::new(1, 0.1), Frame::new(2, 0.2), Frame::new(3, 0.3)
        ];
        assert_eq!(pass_frames_through(&frames, 0.0), frames);
    }

    #[test]
    fn values_bool() {
        let frames = vec![
            Frame::new(true, 0.0), Frame::new(false, 0.1), Frame::new(true, 0.2), Frame::new(true, 0.3)
        ];
        assert_eq!(pass_frames_through(&frames, 0.0), frames);
    }

    #[test]
    fn freq_zero_framecount() {
        let mut meas: Measurement<f64, String> = Measurement::new("origin".to_string(), 0.0);
        meas.measure(0.0, 0.0);
        meas.measure(1.0, 0.1);
        assert_eq!(meas.frames().len(), 2);
    }

    #[test]
    fn freq_nonzero_framecount() {
        let mut meas: Measurement<f64, String> = Measurement::new("origin".to_string(), 0.2);
        meas.measure(0.0, 0.0);
        meas.measure(1.0, 0.1);
        meas.measure(2.0, 0.2);
        assert_eq!(meas.frames().len(), 2);
    }

    #[test]
    fn suspend_resume_zero_freq() {
        let mut meas: Measurement<f64, String> = Measurement::new("origin".to_string(), 0.0);
        meas.measure(0.0, 0.0);
        meas.suspend(0.0);
        meas.measure(1.0, 0.1);
        meas.measure(2.0, 0.2);
        meas.resume(0.2);
        meas.measure(3.0, 0.3);
        assert_eq!(*meas.frames(), vec![Frame::new(0.0, 0.0), Frame::new(3.0, 0.3)])
    }

    #[test]
    fn suspend_resume_nonzero_freq() {
        let mut meas: Measurement<f64, String> = Measurement::new("origin".to_string(), 0.2);
        meas.measure(0.0, 0.0);
        meas.suspend(0.0);
        meas.measure(1.0, 0.1);
        meas.measure(2.0, 0.2);
        meas.resume(0.2);
        meas.measure(3.0, 0.3);
        meas.measure(4.0, 0.4);
        assert_eq!(*meas.frames(), vec![Frame::new(0.0, 0.0), Frame::new(4.0, 0.4)])
    }

    #[test]
    fn frames_by_count() {
        let frames = vec![Frame::new(0.0, 0.0), Frame::new(1.0, 0.1), Frame::new(2.0, 0.2)];
        let meas = apply_frames(&frames, 0.0);
        assert_eq!(meas.frames_by_count(2), frames[1..].iter().collect::<Vec<&Frame<f64>>>())
    }

    #[test]
    fn drain_frames() {
        let frames = vec![Frame::new(0.0, 0.0), Frame::new(1.0, 0.0), Frame::new(2.0, 0.2)];
        let mut meas = apply_frames(&frames, 0.0);
        let returned_frames = meas.drain();
        assert_eq!(returned_frames, frames);
    }

    #[test]
    fn clear_frames() {
        let frames = vec![Frame::new(0.0, 0.0), Frame::new(1.0, 0.0), Frame::new(2.0, 0.2)];
        let mut meas = apply_frames(&frames, 0.0);
        meas.clean(2);
        assert_eq!(meas.frames()[..], frames[1..]);
    }
}
