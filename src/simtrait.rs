use std::any::Any;
use std::collections::HashMap;
use std::hash::BuildHasherDefault;
use rustc_hash::FxHasher;

use units::metric::Meters;

use super::{
    collider::Collider,
    measurement::Property,
};

pub type PropertyFnc = Box<dyn Fn(&dyn SimItem) -> Property + Send>;
pub type PropertyMap = HashMap<String, PropertyFnc, BuildHasherDefault<FxHasher>>;
pub type World = HashMap<String, Box<dyn SimItem>, BuildHasherDefault<FxHasher>>;

/// Objects used in `Simulation` must implement this trait
///
/// We provide the minimal functionality of structure to be simulated. This
/// includes the `sim()` function to progress individual items, the `collider()`
/// which is used in physics simulation and the `orientation` with which we can
/// get the position and rotation in the 2D space.
pub trait SimItem: SimItemClone + Send + Any + Sync {
    /// Simulates one step on the item
    ///
    /// Naturally we have to pass the elpased time (`delta_ms`) which is
    /// expected in millisecs, as well ass the environment the item is 
    /// experiencing in the current step (`world`). We do not consume to world
    /// as when simulating multiple items in a single step every item will need
    /// it.
    ///
    /// We provide a default implementation which is no simulation at all. This
    /// should be quite noticable by the user, as the item will not do anything.
    fn sim(&mut self, _delta_ms: f64, _world: &mut World) {
        // pass
    }

    /// Returns with the collider of the item
    ///
    /// This basically the outer shell of the item, used for physics simulation
    fn collider(&self) -> Option<Collider> {
        //No collider by default
        None
    }

    /// Returns with the Orientation of the item in meters and rads
    ///
    /// This is optional, and used in the physics simulation
    fn orientation(&self) -> Option<Orientation> {
        // By default, no orientation
        None
    }

    /// Returns with the list of properties
    ///
    /// The return value is a hash map containing the property names and their
    /// respective closure. This closure can be used (with the item is input
    /// argument) to retrieve the property value.
    fn properties(&self) -> PropertyMap {
        HashMap::default()
    }
}

/// This trait makes it possible to clone boxed SimItem objects
///
/// By making the `SimItem` trait requiring the `SimItemClone` trait we make
/// the trit objects being capable to be cloned when Boxed. Boxing is required
/// as trait objects may not be `Sized`.
pub trait SimItemClone {
    fn clone_box(&self) -> Box<dyn SimItem>;
    fn as_any(&self) -> Box<dyn Any>;
    fn as_any_ref(&self) -> Box<&dyn Any>;
    fn as_any_mut<'a>(&'a mut self) -> Box<&'a mut dyn Any>;
}

/// Implement `SimItemClone` for any `SimItem`
///
/// Of course this brings in the restriction that clonable `SimItem` stuctures
/// must also be `Clone`. 
impl<T: 'static + SimItem + Clone> SimItemClone for T {
    fn clone_box(&self) -> Box<dyn SimItem> {
        Box::new(self.clone())
    }

    fn as_any(&self) -> Box<dyn Any> {
        Box::new(self.clone())
    }

    fn as_any_ref<'a>(&'a self) -> Box<&'a dyn Any> {
        Box::new(self)
    }

    fn as_any_mut<'a>(&'a mut self) -> Box<&'a mut dyn Any> {
        Box::new(self)
    }
}

/// Two dimensional position and rotation
///
/// This is the base class for physics simulation. With a collider we are able
/// to simulate simple physics.
pub struct Orientation {
    pub x: Meters,
    pub y: Meters,
    pub rot: f64,
}

/// Convenience function to make it easier to dowcast from `SimItem`
///
/// This simply reduces biolerplate code, and also takes care of error handling.
#[macro_export]
macro_rules! dowcast_as_any {
    ($item:ident, $to:ident) => {
        <Box<dyn std::any::Any>>::downcast::<$to>($item.as_any())
            .expect(concat!("Cannot dowcast ", stringify!($item), "to", stringify!($to)))
    };
}

#[macro_export]
macro_rules! dowcast_as_any_ref {
    ($item:ident, $to:ident) => {
        (*$item.as_any_ref()).downcast_ref::<$to>()
            .expect(concat!("Cannot dowcast ", stringify!($item), " to ", stringify!($to)))
    };
}

#[macro_export]
macro_rules! dowcast_as_any_mut {
    ($item:ident, $to:ident) => {
        (*$item.as_any_mut()).downcast_mut::<$to>()
            .expect(concat!("Cannot dowcast ", stringify!($item), " to ", stringify!($to)))
    };
}
