//! Simulation framework implementing a numeric simulation
///
/// 

#[macro_use]
pub mod simtrait;
pub mod simulation;
pub mod measurement;
pub mod collider;

#[cfg(test)]
mod tests {
    use crate::simtrait::World;

    use super::{
        simtrait::{ SimItem, PropertyMap },
        simulation::Simulation,
        measurement::Property,
    };
    use std::collections::HashMap;
    use std::any::Any;
    use std::f64::consts::PI;

    static ANT_NAME: &str = "Ant";
    static ANT_POS_Y: &str = "PositionY";

    #[derive(Clone)]
    struct SinAnt {
        pub x: f64,
        pub y: f64,
    }

    impl Default for SinAnt {
        fn default() -> Self {
            SinAnt { x: 0.0, y: 0.0 }
        }
    }

    impl SimItem for SinAnt {
        fn sim(&mut self, _delta_ms: f64, _world: &mut World) {
            self.x += _delta_ms;
            self.y = self.x.sin();
        }

        fn properties(&self) -> PropertyMap {
            let mut props: PropertyMap = HashMap::default();
            // Position X
            props.insert("PositionX".to_string(), Box::new(|simitem| {
                //let sinant = <Box<dyn Any>>::downcast::<SinAnt>(simitem.as_any())
                //    .expect("Cannot downcast to `SinAnt`");
                let sinant = dowcast_as_any!(simitem, SinAnt);
                Property::Float(sinant.x)
            }));
            // Position X
            props.insert(ANT_POS_Y.to_string(), Box::new(|simitem| {
                let sinant = <Box<dyn Any>>::downcast::<SinAnt>(simitem.as_any())
                    .expect("Cannot downcast to `SinAnt`");
                Property::Float(sinant.y)
            }));
            props
        }
    }

    fn sim_with_single_ant() -> Simulation {
        let mut sim = Simulation::new();
        sim.insert(ANT_NAME, Box::new(SinAnt::default()));
        sim
    }

    #[test]
    fn get_ant_y_max() {
        let mut sim = sim_with_single_ant();
        sim.measure(ANT_NAME, ANT_POS_Y, 0.0).unwrap();
        sim.step(PI/2.0);
        let meas = sim.obtain_measurement(ANT_NAME, ANT_POS_Y).unwrap();
        assert_eq!(meas.values(), vec![&Property::Float(0.0), &Property::Float(1.0)])
    }

    #[test]
    fn get_ant_y_min() {
        let mut sim = sim_with_single_ant();
        sim.measure(ANT_NAME, ANT_POS_Y, 0.0).unwrap();
        sim.step(PI*1.5);
        let meas = sim.obtain_measurement(ANT_NAME, ANT_POS_Y).unwrap();
        assert_eq!(meas.values(), vec![&Property::Float(0.0), &Property::Float(-1.0)])
    }
}
