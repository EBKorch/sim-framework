use std::collections::HashMap;
use std::cmp:: { Eq, PartialEq };
use std::hash::{ Hash, BuildHasherDefault };
use std::rc::Rc;
use std::cell::RefCell;
use rustc_hash::FxHasher;

use crate::simtrait::World;

use super::{
    simtrait::{ SimItem, Orientation, PropertyFnc },
    measurement::{ Property, Measurement },
};

#[derive(Debug)]
pub enum MeasError {
    NoSuchItem,
    NoSuchProp,
}

#[derive(PartialEq)]
pub struct MLink<T> {
    item: T,
    property: String,
}

pub type Items = HashMap<String, Rc<RefCell<Box<dyn SimItem>>>, BuildHasherDefault<FxHasher>>;

/// Stores the simulated items and runs the simulation loop
///
/// Any structure which implements `SimItem` trait can be added to the
/// simulation.Progressing the simulation with a defined delta time can be done
/// with `step()`.
pub struct Simulation {
    items: Items,
    measurements: Vec<(Rc<RefCell<Box<dyn SimItem>>>, Measurement<Property, MLink<String>>, PropertyFnc)>,
    sec: f64,
    history_time: f64,
    cleanup_frequency: usize,
    cleanup_fraction: usize,
    cleanup_counter: usize,
    postproc: Box<dyn FnMut(&mut Items) -> () + Send>
}

unsafe impl Send for Simulation {}

impl Simulation {
    pub fn new() -> Self {
        Simulation {
            items: HashMap::default(),
            measurements: Vec::new(),
            sec: 0.0,
            history_time: -1.0,
            cleanup_frequency: 0,
            cleanup_fraction: 0,
            cleanup_counter: 0,
            postproc: Box::new(|_| {}),
        }
    }

    pub fn set_postproc(&mut self, postproc: Box<dyn FnMut(&mut Items) -> () + Send>) {
        self.postproc = postproc;
    }

    pub fn set_history_time(&mut self, time: f64) {
        self.history_time = time;
    }

    pub fn set_cleanup_frequency(&mut self, frequency: usize) {
        self.cleanup_frequency = frequency;
    }

    pub fn insert(&mut self, name: &str, simitem: Box<dyn SimItem>) {
        self.items.insert(name.into(), Rc::new(RefCell::new(simitem)));
    }

    pub fn remove(&mut self, name: &str) -> Option<Rc<RefCell<Box<dyn SimItem>>>> {
        match self.items.get(name) {
            None => None,
            Some(_) => {
                let mut counter = 0;
                loop {
                    if &self.measurements[counter].1.origin().item == name {
                        let _ = self.measurements.remove(counter);
                    } else if counter < self.measurements.len()-1 {
                        counter += 1;
                    } else {
                        break
                    }
                }
                self.items.remove(name)
            }
        }
    }

    pub fn step(&mut self, delta_ms: f64) {
        // Step the global time axis
        self.sec += delta_ms/1000.0;
        // Create a copy from the current status of every item. This will be
        // passed to every simitem.
        let mut world: World = HashMap::with_capacity_and_hasher(self.items.len(), BuildHasherDefault::<FxHasher>::default());
        for (name, value) in self.items.iter() {
            world.insert(name.clone(), value.borrow().clone_box());
        }
        // Simulate every item
        for (_, item) in self.items.iter_mut() {
            item.borrow_mut().sim(delta_ms, &mut world);
        }
        // Step the cleanup counter: if it would become zero we restart the
        // counting, and recalculate the fraction size as well. In every round,
        // only the `cleanup_frequency` fraction of the total measurements are
        // cleaned up.
        if self.cleanup_counter < 1 {
            self.cleanup_fraction = (self.measurements.len() as f64 / self.cleanup_frequency as f64).ceil() as usize;
            self.cleanup_counter = self.cleanup_frequency;
        } else { self.cleanup_counter -= 1; }
        for (index, (item, meas, fnc)) in &mut self.measurements.iter_mut().enumerate() {
            meas.measure(fnc(item.borrow().as_ref()), self.sec);
            // Check if this measurement is in the currently cleaned up
            // fraction
            if index < self.cleanup_fraction*(self.cleanup_counter+1) && index > self.cleanup_fraction*self.cleanup_counter {
                meas.clean_by_time(self.history_time);
            }
        }
        // Run the post process step
        (*self.postproc)(&mut self.items)
    }

    pub fn measure(&mut self, item: &str, property: &str, samplingtime: f64) -> Result<(), MeasError> {
        let item: String = item.into();
        match self.items.get(&item) {
            None => Err(MeasError::NoSuchItem),
            Some(value) => {
                let locked = value.borrow();
                let mut properties = locked.properties();
                match properties.remove(property) {
                    None => Err(MeasError::NoSuchProp),
                    Some(fnc) => {
                        let link = MLink { item, property: property.to_string() };
                        let mut meas = Measurement::new(link, samplingtime);
                        meas.measure(fnc(locked.as_ref()), self.sec);
                        self.measurements.push((Rc::clone(value), meas, fnc));
                        Ok(())
                    }
                }
            }
        }
    }

    pub fn obtain_measurement(&self, item: &str, property: &str) -> Option<&Measurement<Property, MLink<String>>> {
        let link = MLink { item: item.into(), property: property.to_string() };
        for (_, meas, _) in &self.measurements {
            if meas.origin() == &link {
                return Some(meas)
            }
        }
        None
    }

    pub fn orientations(&self) -> HashMap<&String, Orientation> {
        let mut map = HashMap::new();
        for (name, item) in &self.items {
            if let Some(orientation) = item.borrow().orientation() {
                map.insert(name, orientation);
            }
        }
        map
    }

    pub fn get_item(&self, name: &str) -> Option<&Rc<RefCell<Box<dyn SimItem>>>> {
        self.items.get(name)
    }

    pub fn get_item_mut(&mut self, name: &str) -> Option<&mut Rc<RefCell<Box<dyn SimItem>>>> {
        self.items.get_mut(name)
    }

    pub fn item_count(&self) -> usize {
        self.items.len()
    }

    pub fn iter_items(&self) -> std::collections::hash_map::Iter<String, Rc<RefCell<Box<dyn SimItem>>>> {
        self.items.iter()
    }

    pub fn suspend_measurement(&mut self, item: &str, property: &str) -> Option<bool> {
        let time = self.sec;
        self.apply_to_measurement(
            MLink { item: item.into(), property: property.to_string() },
            |meas| meas.suspend(time)
        )
    }

    pub fn resume_measurement(&mut self, item: &str, property: &str) -> Option<bool> {
        let time = self.sec;
        self.apply_to_measurement(
            MLink { item: item.into(), property: property.to_string() },
            |meas| meas.resume(time)
        )
    }

    fn apply_to_measurement<R, F: Fn(&mut Measurement<Property, MLink<String>>) -> R>(&mut self, link: MLink<String>, fnc: F) -> Option<R> {
        match self.measurements.iter_mut().find_map(|(_, meas, _)| {
            if meas.origin() == &link { Some(meas) } else { None }
        }) {
            None => None,
            Some(meas, ) => Some(fnc(meas)),
        }
    }
}
